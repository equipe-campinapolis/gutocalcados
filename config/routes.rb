Rails.application.routes.draw do
  get 'about/index'
  mount RailsAdmin::Engine => '/site/', as: 'rails_admin'
  resources :products
  root to: 'home#index'
  get 'contact/index'
  get 'home/index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
