class Product < ApplicationRecord
	has_one_attached :image
  	belongs_to :scategory
  	belongs_to :category
  	enum kind: [:ativo, :inativo]
end
